# stage: 1
FROM node:8
WORKDIR /app
COPY . ./
EXPOSE 3000
RUN npm install
CMD npm start
