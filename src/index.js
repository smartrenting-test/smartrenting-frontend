import React from 'react';
import ReactDOM from 'react-dom';import { library } from '@fortawesome/fontawesome-svg-core';
import { faArrowRight, faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import Dashboard from './components/Dashboard/dashboard.component';

import './index.css';

library.add(faArrowRight);
library.add(faArrowLeft);

ReactDOM.render(<Dashboard />, document.getElementById('root'));
