import React from 'react';
import characterService from "../../services/character.service";
import Character from "../Character/character.component";
import Navigation from "../Navigation/navigation.component";
import './dashboard.component.css';
import Loader from "../Loader/loader.component";

export default class Dashboard extends React.Component {
  constructor() {
    super();
    this.state = {
      listCharacters: [],
      offset: 0,
    };

    this.getData = this.getData.bind(this);
    this._handleKeyDown =  this._handleKeyDown.bind(this);
  }

  componentDidMount() {
    this.getData(0);
    document.addEventListener("keydown", this._handleKeyDown, false);
  }

  _handleKeyDown(e) {
    switch (e.keyCode) {
      case 37:
        if (this.state.offset - 1 >= 0) {
          this.getData(this.state.offset - 1);
        }
        break;
      case 39:
        this.getData(this.state.offset + 1);
        break;
      default:
        break;
    }
  }

  getData(offset) {
    this.setState({
      listCharacters: [],
      offset: offset
    });
    characterService.getCharacters(offset).then((data) => {
      this.setState({
        listCharacters: data,
        offset: offset,
      });
    });
  }

  render() {
    return (
      <div className="dashboard">
        <header className="dashboard-header">
          <p>
            Marvel's superheroes trombinoscope
          </p>
        </header>

        <div className="characters">
          {
            this.state.listCharacters.length !== 0 ?
            this.state.listCharacters.map((element, i) => (
              <Character character={element} key={i}/>
            )) : (
              <Loader/>
              )
          }
        </div>

        <footer className="dashboard-footer">
          <Navigation offset={this.state.offset}
                      changeOffset={this.getData}/>
        </footer>
      </div>
    )};
}