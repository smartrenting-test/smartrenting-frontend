import React from 'react';
import './character.component.css';

export default class Caracter extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="character">
        <img className="character-img"
             src={this.props.character._imageSrc}
             alt="Loading" />

        <div className="character-name">{this.props.character._name}</div>
      </div>
    )
  }
}