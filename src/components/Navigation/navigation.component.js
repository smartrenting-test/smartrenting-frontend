import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './navigation.component.css';

export default class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      offset: this.props.offset
    };
    this.handleClickMinus = this.updateOffsetMinus.bind(this);
    this.handleClickPlus = this.updateOffsetPlus.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this._handleKeyDown = this._handleKeyDown.bind(this);
  }

  componentDidUpdate(oldProps) {
    const newProps = this.props;
    if (newProps !== oldProps) {
      this.setState({...newProps});
    }
  }

  _handleKeyDown(e) {
    if (e.key === 'Enter') {
      e.target.blur();
      this.props.changeOffset(this.state.offset);
    }
  }

  handleChange(event) {
    this.setState({offset: event.target.value});
  }

  select = (e) => e.target.select();

  updateOffsetPlus(e) {
    this.props.changeOffset(this.props.offset + 1);
  }

  updateOffsetMinus(e) {
    if (this.props.offset - 1 >= 0) {
      this.props.changeOffset(this.props.offset - 1);
    }
  }

  render() {
    return (
      <div className="navigation">
        <span className="navigation-btn" onClick={this.handleClickMinus}><FontAwesomeIcon icon="arrow-left" /></span>
        <input type="text" className="navigation-offset"
               value={this.state.offset}
               onClick={this.select}
               onChange={this.handleChange}
               onKeyDown={this._handleKeyDown}/>
        <span className="navigation-btn" onClick={this.handleClickPlus}><FontAwesomeIcon icon="arrow-right" /></span>
      </div>
    )
  }
}