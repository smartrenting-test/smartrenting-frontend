import React from 'react';
import './loader.component.css';

export default class Loader extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="loader">
        <div className="cursor"></div>
        <div className="cursor2"></div>
      </div>
    )
  }
}