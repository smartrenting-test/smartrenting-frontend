import config from '../config/config';
const http = require('http');

export default class Requests {
  static async get(url) {
    const options = {
      hostname: config.URLS.API,
      port: config.URLS.API_PORT,
      path: url,
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': 'allow',
      },
      body: undefined
    };
    console.log(options);
    return new Promise((resolve, reject) => {
      const req =http.request(options, (res) =>{
        let body = '';

        res.on('data', (d) => {
          body += d;
        });

        res.on('end', () => {
          resolve(JSON.parse(body));
        });

        res.on('error', (error) => {
          reject(error);
        });
      });

      req.on('error', (e) => {
        console.error(`problem with request: ${e.message}`);
      });
      req.end();
    });
  }
}