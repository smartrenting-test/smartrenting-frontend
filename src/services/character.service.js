import Request from "./requests";

const url ={
  getWithOffset: '/characters/offset/'
};

async function getCharacters(offset) {
  const service = `${url.getWithOffset}${offset}`;
  return Request.get(service);
}

export default {
  getCharacters
}