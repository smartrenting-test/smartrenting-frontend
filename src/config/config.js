export default {
  URLS: {
    API: process.env.REACT_APP_API,
    API_PORT: process.env.REACT_APP_API_PORT
  }
}